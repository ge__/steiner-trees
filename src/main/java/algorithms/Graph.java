package algorithms;

import java.util.*;
import java.util.stream.Collectors;

public class Graph {
    private final Map<Integer, Set<Edge>> graphMap;

    public Graph() {
        this.graphMap = new HashMap<>();
    }

    public Graph(Map<Integer, Set<Edge>> graphMap) {
        this.graphMap = graphMap;
    }

    public Graph(double[][] matrix) {
        graphMap = new HashMap<>();
        for (int i = 0; i < matrix.length; i++)
            for (int j = 0; j < matrix.length; j++)
                if (matrix[i][j] != 0)
                    addEdge(new Edge(i, j, matrix[i][j]));
    }

    public Set<Edge> getEdges() {
        Set<Edge> edges = new HashSet<>();
        for (Set<Edge> vertexEdges : graphMap.values()) {
            edges.addAll(vertexEdges);
        }
        //Removes duplicate at the end.
        return edges.stream().filter(edge->edge.from()>edge.to()).collect(Collectors.toSet());
    }


    public Map<Integer, Set<Edge>> getGraphMap() {
        return graphMap;
    }

    public boolean isInGraph(int vertex) {
        return graphMap.containsKey(vertex);
    }

    public void addVertex(int vertex) {
        graphMap.putIfAbsent(vertex, new HashSet<>());
    }

    public Set<Integer> getNeighborsSet(int vertex) {
        return new HashSet<>(graphMap.get(vertex).stream().map(edge -> edge.to()).collect(Collectors.toSet()));
    }

    public Set<Integer> getVertices() {
        return new HashSet<>(graphMap.keySet());
    }

    public void addEdge(Edge edge) {
        int vertex1 = edge.from();
        int vertex2 = edge.to();
        Edge edge1 = edge;
        //The edge in the other direction
        Edge edge2 = new Edge(vertex2, vertex1, edge.weight());

        if (graphMap.containsKey(vertex1)) {
            graphMap.get(vertex1).add(edge1);
        } else {
            Set<Edge> set = new HashSet<>();
            set.add(edge1);
            graphMap.put(vertex1, set);
        }
        if (graphMap.containsKey(vertex2)) {
            graphMap.get(vertex2).add(edge2);
        } else {
            Set<Edge> set = new HashSet<>();
            set.add(edge2);
            graphMap.put(vertex2, set);
        }

    }


    public void removeEdge(int vertex1, int vertex2) {
        graphMap.get(vertex1).remove(vertex2);
        if (graphMap.get(vertex1).size() == 0)
            graphMap.remove(vertex1);

        graphMap.get(vertex2).remove(vertex1);
        if (graphMap.get(vertex2).size() == 0)
            graphMap.remove(vertex2);
    }

    public double getTotalWeight() {
        double totalWeight = 0;
        for (int from : graphMap.keySet()) {
            for (Edge edge : graphMap.get(from)) {
                //To not count 2 times each edge
                if (edge.to() < edge.from())
                    totalWeight += edge.weight();
            }
        }
        return totalWeight;
    }

    public List<Edge> getSortedEdgeList() {
        Set<Edge> edgeSet=getEdges();
        // Sort edges in decreasing order of weight.
        List<Edge> edgeList = edgeSet.stream().sorted((e1, e2) -> (int) (e2.weight() - e1.weight())).toList();
        return edgeList;
    }


}
