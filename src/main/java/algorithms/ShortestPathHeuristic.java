package algorithms;

import main.PathInformation;

import java.util.*;

public class ShortestPathHeuristic extends SteinerAlgorithm {
    //We represent a subgraph with a map that links each node to its neighbors.
    private Graph T = new Graph();
    private final PathInformation[][] minimalDistanceMatrix;

    public ShortestPathHeuristic(int k, double[][] matrix, PathInformation[][] minimalDistanceMatrix) {
        super(k, matrix);
        this.minimalDistanceMatrix = minimalDistanceMatrix;
    }

    @Override
    public double resolve() {
        //Initializes T.
        int firstTerminal = K.stream().findFirst().get();
        Set<Integer> nonTreatedTerminals = new HashSet<>(K);
        nonTreatedTerminals.remove(firstTerminal);
        //Just a vertex without any edges for now.
        T.addVertex(firstTerminal);


        while (!nonTreatedTerminals.isEmpty()) {
            //Computes the terminal with the shortest distance.
            double min = Double.MAX_VALUE;
            PathInformation closestTerminalPath = null;
            for (int treeVertex : T.getVertices())
                for (int terminalVertex : nonTreatedTerminals) {
                    PathInformation pathInfo = minimalDistanceMatrix[treeVertex][terminalVertex];
                    if (pathInfo.totalDistance() < min) {
                        min = pathInfo.totalDistance();
                        closestTerminalPath = pathInfo;
                    }
                }

            //We now update the tree T.
            List<Edge> path = closestTerminalPath.path();
            for (Edge edge : path)
                T.addEdge(edge);

            //We update K
            for (Edge edge : path) {
                if (nonTreatedTerminals.contains(edge.from()))
                    nonTreatedTerminals.remove(edge.from());

                if (nonTreatedTerminals.contains(edge.to()))
                    nonTreatedTerminals.remove(edge.to());
            }
            //We compute the MST within T.
            T = new Kruskal(T, n).getMST();

            //We remove non-terminal leaves.
            removeNonTerminalLeaves(T);

        }
        return T.getTotalWeight();
    }

}
