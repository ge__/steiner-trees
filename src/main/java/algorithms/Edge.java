package algorithms;

public record Edge(int from, int to, double weight) {
}