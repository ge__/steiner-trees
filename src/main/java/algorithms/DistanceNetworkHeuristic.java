package algorithms;

import main.PathInformation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DistanceNetworkHeuristic extends SteinerAlgorithm {
    private final PathInformation[][] shortestDistanceMatrix;
    private final Graph D;

    public DistanceNetworkHeuristic(int k, double[][] matrix, PathInformation[][] shortestDistanceMatrix) {
        super(k, matrix);
        this.shortestDistanceMatrix = shortestDistanceMatrix;

        //Creates D(K)
        double[][] dMatrix = new double[k][k];
        for (int i = 0; i < k; i++)
            for (int j = 0; j < k; j++)
                dMatrix[i][j] = shortestDistanceMatrix[i][j].totalDistance();
        //Creates D
        D = new Graph(dMatrix);
    }

    @Override
    public double resolve() {
        //Computes a MST for D
        Graph MST = new Kruskal(D,n).getMST();

        //Transform MST into a subgraph of G.
        Graph Td = new Graph();
        //replace every edge by the corresponding path.
        for(Edge edge: MST.getEdges())
            shortestDistanceMatrix[edge.from()][edge.to()].path().forEach(pathEdge->Td.addEdge(pathEdge));

        //Compute a MST on Td
        Graph T= new Kruskal(Td,n).getMST();

        //Remove non-terminal leaves
        removeNonTerminalLeaves(T);

        return T.getTotalWeight();
    }
}
