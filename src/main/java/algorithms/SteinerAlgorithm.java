package algorithms;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class SteinerAlgorithm {
    protected final int k;
    protected final double[][] matrix;
    protected final int n;
    protected final Set<Integer> G;
    protected final Set<Integer> K;


    public SteinerAlgorithm(int k, double[][] matrix) {
        this.k = k;
        K = new HashSet<>();
        for (int i = 0; i < k; i++) {
            K.add(i);
        }
        this.matrix = matrix;
        n = matrix.length;
        G = new HashSet<>();
        for (int i = 0; i < n; i++) {
            G.add(i);
        }
    }


    /**
     * @return The complementary of the second set in the first set (set1/set2)
     */
    protected Set<Integer> complementary(Set<Integer> set1, Set<Integer> set2) {
        return set1.stream().filter(val -> !set2.contains(val)).collect(Collectors.toSet());
    }


    /**
     * Gets the adjacency matrix corresponding to the subset.
     */
    protected double[][] getSubsetMatrix(ArrayList<Integer> subset) {
        //At the end we copy the new array without the other indexes
        int result_size = subset.size();
        double[][] result = new double[result_size][result_size];
        int i_index = 0;
        for (int i = 0; i < n; i++) {
            if (subset.contains(i)) {
                int j_index = 0;
                for (int j = 0; j < n; j++) {
                    if (subset.contains(j)) {
                        result[i_index][j_index] = matrix[i][j];
                        j_index++;
                    }
                }
                i_index++;
            }
        }
        return result;
    }

    protected void removeNonTerminalLeaves(Graph T) {
        for (int vertex : T.getVertices())
            if (!K.contains(vertex) && T.getNeighborsSet(vertex).size() == 1)
                removeRecursively(T,vertex);
    }

    private void removeRecursively(Graph T,int vertex) {
        //The parent of the leaf
        int parent = T.getNeighborsSet(vertex).stream().findFirst().get();
        T.removeEdge(vertex, parent);

        if (T.getNeighborsSet(parent).size() == 1 && !K.contains(parent))
            removeRecursively(T,parent);
    }

    public abstract double resolve();


}
