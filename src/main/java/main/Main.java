package main;

import algorithms.DistanceNetworkHeuristic;
import algorithms.DreyfusWagner;
import algorithms.ShortestPathHeuristic;
import parsing.GraphParser;
import parsing.ParsingResult;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    // Edges array
    private static int[][] edges;

    // Number of vertices
    private static int n;

    // Adjacency matrix
    private static int[][] matrix;

    // Union-find data structure
    private static int[] parent;


    public static void main(String[] args) throws IOException {
        GraphParser graphParser = new GraphParser();

        for (int i = 1; i < 6; i++) {
            long t0 = System.currentTimeMillis();
            File file = new File(getPath(i));
            ParsingResult scenario = graphParser.parse(file);
            int k = scenario.k();
            Preprocessor preprocessor = new Preprocessor(scenario.adjacencyMatrix(), scenario.k());
            preprocessor.preprocess();
            double[][] matrix = preprocessor.getMatrix();


            PathInformation[][] minimalDistanceMatrix = preprocessor.getMinimalDistanceMatrix();
            System.out.println("Matrix:");
            System.out.println(matrixToString(matrix));
            System.out.println("Minimal Distance Matrix:");
            System.out.println(matrixToString(minimalDistanceMatrix));

            System.out.println("Preprocessing Time for example" + i + " " + (System.currentTimeMillis() - t0));

            System.out.println("------");
            System.out.println("Dreyfus Wagner");
            long t1 = System.currentTimeMillis();
            DreyfusWagner dreyfusWagner = new DreyfusWagner(k, matrix, minimalDistanceMatrix);
            System.out.println("Result:" + dreyfusWagner.resolve());
            System.out.println("Time:" + (System.currentTimeMillis() - t1));
            System.out.println("---------------");

            System.out.println("Shortest Path Heuristic");
            long t2 = System.currentTimeMillis();
            ShortestPathHeuristic shortestPathHeuristic = new ShortestPathHeuristic(k, matrix, minimalDistanceMatrix);
            System.out.println("Result:" + shortestPathHeuristic.resolve());
            System.out.println("Time:" + (System.currentTimeMillis() - t2));
            System.out.println("---------------");

            System.out.println("Distance Network Heuristic");
            long t3 = System.currentTimeMillis();
            DistanceNetworkHeuristic distanceNetworkHeuristic = new DistanceNetworkHeuristic(k, matrix, minimalDistanceMatrix);
            System.out.println("Result:" + distanceNetworkHeuristic.resolve());
            System.out.println("Time:" + (System.currentTimeMillis() - t3));
            System.out.println("---------------");

            System.out.println();
            System.out.println("-----------------------");
            System.out.println();

        }
    }


    // Finds the root of the set that x belongs to
    private static int find(int x) {
        if (parent[x] != x) {
            parent[x] = find(parent[x]);
        }
        return parent[x];
    }

    // Unions the sets that x and y belong to
    private static void union(int x, int y) {
        int rootX = find(x);
        int rootY = find(y);
        if (rootX != rootY) {
            parent[rootX] = rootY;
        }
    }

    // Kruskal's algorithm for finding the maximum spanning tree
    private static void kruskal() {
        // Initialize the edges array
        edges = new int[n - 1][3];

        // Sort the edges in non-decreasing order of weight
        Arrays.sort(matrix, (a, b) -> a[2] - b[2]);

        // Initialize the union-find data structure
        parent = new int[n];
        for (int i = 0; i < n; i++) {
            parent[i] = i;
        }

        // Keep track of the number of edges added to the MST
        int count = 0;

        // Iterate over the edges in non-decreasing order of weight
        for (int[] edge : matrix) {
            // If the current edge connects two vertices in different sets
            if (find(edge[0]) != find(edge[1])) {
                // Add the edge to the MST
                edges[count] = edge;
                count++;

                // Union the sets that the edge connects
                union(edge[0], edge[1]);

                // If we have added n-1 edges, we have found the MST
                if (count == n - 1) {
                    break;
                }
            }
        }
    }


    public static String getPath(int i) {
        return "src/scenarios/scenario" + i + ".txt";
    }


    public static String matrixToString(double[][] matrix) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                sb.append(matrix[i][j]);
                sb.append(" ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    public static String matrixToString(PathInformation[][] matrix) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] != null)
                    sb.append(matrix[i][j].totalDistance());
                else
                    sb.append("null");

                sb.append(" ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }


    public static List<Integer> getNeighborsList(double[][] matrix, int vertex) {
        List<Integer> neighborsList = new ArrayList<>();
        for (int j = 0; j < matrix.length; j++) {
            if (matrix[vertex][j] != 0)
                neighborsList.add(j);
        }
        return neighborsList;
    }

}
