package main;

import algorithms.Edge;

import java.util.List;

public record PathInformation(double totalDistance, List<Edge> path) implements Comparable {


    public int getStartVertex() {
        return path.get(0).from();
    }

    public int getEndVertex() {
        return path.get(path.size() - 1).to();
    }

    /**
     * Used for priority lists.
     */
    @Override
    public int compareTo(Object o) {
        if (o instanceof PathInformation pointInformation) {
            return totalDistance - pointInformation.totalDistance < 0 ? -1 : 1;
        }
        return 0;
    }
}