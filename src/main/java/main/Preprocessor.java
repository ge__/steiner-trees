package main;

import algorithms.Edge;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class Preprocessor {
    protected double[][] matrix;
    private final int n;
    private final int k;
    private PathInformation[][] minimalDistanceMatrix;

    public Preprocessor(double[][] matrix, int k) {
        this.matrix = matrix;
        this.n = matrix.length;
        minimalDistanceMatrix = new PathInformation[n][n];
        this.k = k;
    }

    public double[][] getMatrix() {
        return matrix;
    }

    public PathInformation[][] getMinimalDistanceMatrix() {
        return minimalDistanceMatrix;
    }

    public void preprocess() {
        computeMinimalDistanceMatrix(matrix);
        ArrayList<Integer> removeIndexes = new ArrayList<>();
        //Goes through all the non-terminal vertexes
        for (int i = k; i < n; i++) {
            int degree = Main.getNeighborsList(matrix, i).size();
            if (degree <= 1)
                removeIndexes.add(i);
            if (degree == 2) {
                processDegree2Edge(removeIndexes, i);
            }
        }
        //We remove long edges if the weight is greater than the minimal distance.
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (areConnected(i, j) && matrix[i][j] > minimalDistanceMatrix[i][j].totalDistance())
                    matrix[i][j] = 0;
            }
        }
        matrix = copy(matrix, removeIndexes);
        minimalDistanceMatrix = new PathInformation[matrix.length][matrix.length];
        computeMinimalDistanceMatrix(matrix);
    }

    private void processDegree2Edge(ArrayList<Integer> removeIndexes, int vertex) {
        List<Integer> neighbors = Main.getNeighborsList(matrix, vertex);
        int vertex1 = neighbors.get(0);
        int vertex2 = neighbors.get(1);
        if (areConnected(vertex1, vertex2)) {
            double weight1 = matrix[vertex][vertex1];
            double weight2 = matrix[vertex][vertex2];
            double weight = matrix[vertex1][vertex2];
            if (weight1 + weight2 >= weight)
                if (!removeIndexes.contains(vertex))
                    //Remove the vertex
                    removeIndexes.add(vertex);
                else
                    //Remove the edge e
                    matrix[vertex1][vertex2] = 0;
        }
    }


    private boolean areConnected(int vertex1, int vertex2) {
        return matrix[vertex1][vertex2] != 0;
    }

    private double[][] copy(double[][] matrix, ArrayList<Integer> removeIndexes) {
        //At the end we copy the new array without the other indexes
        int result_size = n - removeIndexes.size();

        double[][] result = new double[result_size][result_size];
        int i_index = 0;
        for (int i = 0; i < n; i++) {
            if (!removeIndexes.contains(i)) {
                int j_index = 0;
                for (int j = 0; j < n; j++) {
                    if (!removeIndexes.contains(j)) {
                        result[i_index][j_index] = matrix[i][j];
                        j_index++;
                    }
                }
                i_index++;
            }
        }
        return result;
    }

    private PathInformation[][] copy(PathInformation[][] matrix, ArrayList<Integer> removeIndexes) {
        //At the end we copy the new array without the other indexes
        int result_size = n - removeIndexes.size();

        PathInformation[][] result = new PathInformation[result_size][result_size];
        int i_index = 0;
        for (int i = 0; i < n; i++) {
            if (!removeIndexes.contains(i)) {
                int j_index = 0;
                for (int j = 0; j < n; j++) {
                    if (!removeIndexes.contains(j)) {
                        result[i_index][j_index] = matrix[i][j];
                        j_index++;
                    }
                }
                i_index++;
            }
        }
        return result;
    }


    private void djikstraFrom(int initialVertex, double[][] matrix) {
        PriorityQueue<PathInformation> priorityQueue = new PriorityQueue<>();
        //Setups the priorityQueue.
        for (int vertex : Main.getNeighborsList(matrix, initialVertex)) {
            List<Edge> arrayList = new ArrayList();
            arrayList.add(new Edge(initialVertex, vertex, matrix[initialVertex][vertex]));
            priorityQueue.add(new PathInformation(matrix[initialVertex][vertex], arrayList));
        }

        while (!priorityQueue.isEmpty()) {
            PathInformation nextPath = priorityQueue.poll();


            //If the vertex has already been visited
            if (minimalDistanceMatrix[initialVertex][nextPath.getEndVertex()] != null)
                continue;
            //Add the vertex to the minimalDistanceMatrix
            minimalDistanceMatrix[initialVertex][nextPath.getEndVertex()] = nextPath;
            //Add all the neighbors in the priority queue if they have not been visited
            for (int neighbor : Main.getNeighborsList(matrix, nextPath.getEndVertex())) {
               //If the vertex has not been visited yet we add it.
                if (minimalDistanceMatrix[initialVertex][neighbor] == null) {
                    List<Edge> newPath = new ArrayList(nextPath.path());
                    newPath.add(new Edge(nextPath.getEndVertex(), neighbor, matrix[neighbor][nextPath.getEndVertex()]));
                    //Adds the new vertex with its distance to the priority queue.

                    priorityQueue.add(new PathInformation(nextPath.totalDistance() + matrix[neighbor][nextPath.getEndVertex()], newPath));
                }
            }

        }
    }

    private void computeMinimalDistanceMatrix(double[][] matrix) {
        //Setup diagonal element of minimalDistanceMatrix
        for (int i = 0; i < matrix.length; i++) {
            minimalDistanceMatrix[i][i] = new PathInformation(0, new ArrayList<>());
        }

        for (int i = 0; i < matrix.length; i++)
            djikstraFrom(i, matrix);
    }


}
